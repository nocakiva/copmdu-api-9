﻿using System.Collections.Generic;

namespace COPMDU.Domain.Entity
{
    public class Diagnostic 
    {
        public string IdTicket { get; set; }
        public string IdTechnician { get; set; }
        public int IdSoftware { get; set; }
        public int IdFeature { get; set; }
        public string Machine { get; set; }
        public string IP { get; set; }
        public string Message { get; set; }

        public TicketResponse Information { get; set; }
    }

    public class Servico
    {
        public int id { get; set; }
    }

    public class Abrangencia
    {
        public string tipo { get; set; }
        public string valor { get; set; }
    }

    public class TicketResponse
    {
        public string ticket { get; set; }
        public string cidade { get; set; }
        public string cidade_id { get; set; }
        public string cluster_id { get; set; }
        public string subcluster_id { get; set; }
        public string empresa { get; set; }
        public string ura { get; set; }
        public string crn { get; set; }
        public long data_ini { get; set; }
        public long data_prev { get; set; }
        public List<Servico> servicos { get; set; }
        public string sintoma { get; set; }
        public string sintoma_id { get; set; }
        public string natureza { get; set; }
        public string gmud { get; set; }
        public string grupo { get; set; }
        public string usuario { get; set; }
        public string titulo { get; set; }
        public string obs { get; set; }
        public List<Abrangencia> abrangencias { get; set; }
        public string rec { get; set; }
        public string reg { get; set; }
        public string cod_imovel_mdu { get; set; }
        public string titulo_ticket { get; set; }
        public string ticket_descricao { get; set; }
        public string data_ini_status { get; set; }
        public string data_prev_status { get; set; }
        public int snord { get; set; }
        public string TechnnicianId { get; set; }
        public string Contract { get; set; }
        public string Notification { get; set; }
    }

    public class OrderResponse
    {

    }
}
