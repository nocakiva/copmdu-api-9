﻿using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly CopMduDbContext _db;
        private readonly ILogRepository _logRepository;

        public UserRepository(CopMduDbContext db, ILogRepository logRepository)
        {
            _db = db;
            _logRepository = logRepository;
        }

        public IEnumerable<User> GetAll()
        {
            return _db.User;
        }

        public User Get(int id)
        {
            return _db.User.Find(id);
        }

        public void Add(User entity)
        {
            _db.Add(entity);
            _db.SaveChanges();
        }

        public void Update(User entity)
        {
            _db.Update(entity);
            _db.SaveChanges();
        }

        public void Delete(User entity)
        {
            _db.Remove(entity);
            _db.SaveChanges();
        }

        public User Authenticate(Login login)
        {
            var user = _db.User.Where(u => u.Username.Equals(login.Username) && u.Password.Equals(login.Password)).FirstOrDefault();

            if (user == null)
                _logRepository.Add(new Log()
                {
                    Message = $"Não foi possível autenticar o usuário {login.Username}.",
                });

            return user;
        }

        public List<string> GetUsername(string partial) =>
            _db.User
                .Where(u => u.Username.Contains(partial))
                .OrderBy(u => u.Username)
                .Select(u => u.Username)
                .ToList();
    }
}
