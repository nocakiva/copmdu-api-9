﻿using COPMDU.Infrastructure.ClassAttribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain.Notification
{
    class CellList
    {
        [PageResultListProperty(@"<th>IE4</th>[\w\W]+?<tr>([\w\W]+)</tr>")]
        public List<Cell> Cells { get; set; }
    }
}
